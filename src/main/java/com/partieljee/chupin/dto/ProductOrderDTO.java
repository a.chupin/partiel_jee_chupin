package com.partieljee.chupin.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class ProductOrderDTO.
 */
@Table
@Entity
public class ProductOrderDTO {

	/** The id productorder. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long id_productorder;
	
	/** The id product. */
	private Long id_product;
	
	/** The id order. */
	private Long id_order;
	
	/** The quantity. */
	private int	quantity;

	/**
	 * Gets the id productorder.
	 *
	 * @return the id productorder
	 */
	public Long getId_productorder() {
		return id_productorder;
	}

	/**
	 * Sets the id productorder.
	 *
	 * @param id_productorder the new id productorder
	 */
	public void setId_productorder(Long id_productorder) {
		this.id_productorder = id_productorder;
	}

	/**
	 * Gets the id product.
	 *
	 * @return the id product
	 */
	public Long getId_product() {
		return id_product;
	}

	/**
	 * Sets the id product.
	 *
	 * @param id_product the new id product
	 */
	public void setId_product(Long id_product) {
		this.id_product = id_product;
	}

	/**
	 * Gets the id order.
	 *
	 * @return the id order
	 */
	public Long getId_order() {
		return id_order;
	}

	/**
	 * Sets the id order.
	 *
	 * @param id_order the new id order
	 */
	public void setId_order(Long id_order) {
		this.id_order = id_order;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * Instantiates a new product order DTO.
	 */
	public ProductOrderDTO () {}
	
	/**
	 * Instantiates a new product order DTO.
	 *
	 * @param id_productorder the id productorder
	 * @param id_product the id product
	 * @param id_order the id order
	 * @param quantity the quantity
	 */
	public ProductOrderDTO (Long id_productorder, Long id_product, Long id_order, int quantity) {
		this.id_productorder = id_productorder;
		this.id_product = id_product;
		this.id_order = id_order;
		this.quantity = quantity;
	}
	
}
