package com.partieljee.chupin;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * The Class Application.
 */
@ApplicationPath("app")
public class Application extends ResourceConfig {
	
	/**
	 * Instantiates a new application.
	 */
	public Application() {
		packages("com.partieljee.chupin");
	}
}
