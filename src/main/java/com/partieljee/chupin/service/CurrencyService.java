package com.partieljee.chupin.service;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.partieljee.chupin.entity.User;
import com.partieljee.chupin.orm.PersistenceManager;

@Stateless
@Path("/currency")
@LocalBean
public class CurrencyService {
	
	
	/**
	 * API method to create a new currency
	 *	  
	 * @param currency (JSON format)
	 * @return Response 
	 */
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createUser(User currency) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			em.getTransaction().begin();
			em.persist(currency);
			em.getTransaction().commit();
			
			return Response.ok(MediaType.APPLICATION_JSON).build();
			
		}
		
	}
	
	/**
	 * API method to find a currency by id
	 * 
	 * @param id
	 * @return User (JSON format)
	 */
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public User findUser(@QueryParam("id") Long id) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			User p = em.find(User.class, id);
			
			return p;
			
		}
		
	}
	
	/**
	 * API method to update a currency
	 *	  
	 * @param currency (JSON format)
	 * @return Response 
	 */
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUser(User currency) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			em.getTransaction().begin();
			em.merge(currency);
			em.getTransaction().commit();
			
			return Response.ok(MediaType.APPLICATION_JSON).build();
			
		}
		
	}
	
	
	/**
	 * API method to delete a currency by id
	 *	  
	 * @param PathParam currency_id
	 * @return Response 
	 */
	@DELETE
	@Path("/delete/{currency_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteUser(@PathParam("currency_id") Long id) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			em.getTransaction().begin();
			User p = em.find(User.class, id);
			em.remove(p);
			em.getTransaction().commit();
			
			return Response.ok(MediaType.APPLICATION_JSON).build();
			
		} 
		
	}
	
	
	public com.partieljee.chupin.orm.ScopedEntityManager getScopedEntityManager() {
		return PersistenceManager.getInstance().getScopedEntityManagerFactory().createScopedEntityManager();
	}
}
