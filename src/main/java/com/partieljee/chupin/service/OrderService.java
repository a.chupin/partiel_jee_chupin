package com.partieljee.chupin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.partieljee.chupin.entity.Order;
import com.partieljee.chupin.entity.ProductOrder;
import com.partieljee.chupin.orm.PersistenceManager;

@Stateless
@Path("/order")
@LocalBean
public class OrderService {
	
	
	/**
	 * API method to create a new order
	 *	  
	 * @param order (JSON format)
	 * @return Response 
	 */
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createOrder(Order order) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			em.getTransaction().begin();
			em.persist(order);
			em.getTransaction().commit();
			
			return Response.ok(MediaType.APPLICATION_JSON).build();
			
		}
		
	}
	
	/**
	 * API method to find products list by order id
	 * 
	 * @param id
	 * @return Order (JSON format)
	 */
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProductOrder> findOrder(@QueryParam("id") Long id) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			//Query to select all product in the order with the id in parameter of the function
			TypedQuery<ProductOrder> query = em.createQuery("SELECT po FROM ProductOrder po WHERE id_order = ?", ProductOrder.class);
			query.setParameter(1, id);

			List<ProductOrder> productsList = query.getResultList();
			
			//generate a JSON with all products
			return productsList;
			
		}
		
	}
	
	/**
	 * API method to update an order 
	 *	  
	 * @param order (JSON format)
	 * @return Response 
	 */
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOrder(ProductOrder ProductOrder) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			em.getTransaction().begin();
			em.merge(ProductOrder);
			em.getTransaction().commit();
			
			return Response.ok(MediaType.APPLICATION_JSON).build();
			
		}
		
	}
	
	
	/**
	 * API method to delete an order by id
	 * it delete first all records in ProductOrder table with the same order id
	 *	  
	 * @param PathParam order_id
	 * @return Response 
	 */
	@DELETE
	@Path("/delete/{order_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteOrder(@PathParam("order_id") Long id) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {
			
			Query query = em.createNamedQuery("DELETE FROM ProductOrder WHERE id_order = ?");
			query.setParameter(1, id);

			query.executeUpdate();

			em.getTransaction().begin();
			Order p = em.find(Order.class, id);
			em.remove(p);
			em.getTransaction().commit();
			
			return Response.ok(MediaType.APPLICATION_JSON).build();
			
		}
		
	}
	
	
	public com.partieljee.chupin.orm.ScopedEntityManager getScopedEntityManager() {
		return PersistenceManager.getInstance().getScopedEntityManagerFactory().createScopedEntityManager();
	}
}
