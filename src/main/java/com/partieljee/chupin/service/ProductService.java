package com.partieljee.chupin.service;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.partieljee.chupin.entity.Product;
import com.partieljee.chupin.orm.PersistenceManager;

@Stateless
@Path("/product")
@LocalBean
public class ProductService {
	
	
	/**
	 * API method to create a new product
	 *	  
	 * @param product (JSON format)
	 * @return Response 
	 */
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createProduct(Product product) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			em.getTransaction().begin();
			em.persist(product);
			em.getTransaction().commit();
			
			return Response.ok(MediaType.APPLICATION_JSON).build();
			
		}
		
	}
	
	/**
	 * API method to find a product by id
	 * 
	 * @param id
	 * @return Product (JSON format)
	 */
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Product findProduct(@QueryParam("id") Long id) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			Product p = em.find(Product.class, id);
			
			return p;
			
		}
		
	}
	
	/**
	 * API method to update a product
	 *	  
	 * @param product (JSON format)
	 * @return Response 
	 */
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProduct(Product product) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {

			em.getTransaction().begin();
			em.merge(product);
			em.getTransaction().commit();
			
			return Response.ok(MediaType.APPLICATION_JSON).build();
			
		}
		
	}
	

	
	
	public com.partieljee.chupin.orm.ScopedEntityManager getScopedEntityManager() {
		return PersistenceManager.getInstance().getScopedEntityManagerFactory().createScopedEntityManager();
	}
}
