package com.partieljee.chupin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.partieljee.chupin.entity.Order;
import com.partieljee.chupin.entity.Payment;
import com.partieljee.chupin.entity.Product;
import com.partieljee.chupin.entity.ProductOrder;
import com.partieljee.chupin.entity.Payment;
import com.partieljee.chupin.orm.PersistenceManager;

@Stateless
@Path("/payment")
@LocalBean
public class PaymentService {
	
	
	/**
	 * API method to create a new payment
	 *	  
	 * @param payment (JSON format)
	 * @return Response 
	 */
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createPayment(Order order) {
		
		try (com.partieljee.chupin.orm.ScopedEntityManager em = getScopedEntityManager()) {
			
			//retrieve the list of all ordered products
			TypedQuery<ProductOrder> query = em.createQuery("SELECT po FROM ProductOrder po WHERE id_productorder = ?", ProductOrder.class);
			query.setParameter(1, order.getId_order());

			List<ProductOrder> productsList = query.getResultList();
			
			double totalPrice = 0;
			
			//for each products:
			//remove the quantity in stock
			//count the total price
			for(ProductOrder product : productsList) {
				
				Product p = em.find(Product.class, product.getId_product());
				totalPrice = totalPrice + p.getPrice() * product.getQuantity();
				
				int oldQuantity = p.getStock();
				
				Query query2 = em.createQuery("UPDATE Product SET quantity = ? WHERE id_product = ?", ProductOrder.class);
				query2.setParameter(1, oldQuantity - product.getQuantity());
				query2.setParameter(2, p.getId_product());
				
				query2.executeUpdate();
				
			}
			
			return Response.ok(MediaType.APPLICATION_JSON).build();
			
		}
		
	}
	
	
	
	
	public com.partieljee.chupin.orm.ScopedEntityManager getScopedEntityManager() {
		return PersistenceManager.getInstance().getScopedEntityManagerFactory().createScopedEntityManager();
	}
}
