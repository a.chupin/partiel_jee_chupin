package com.partieljee.chupin.orm;


import javax.persistence.EntityManager;


/**
 * The Interface ScopedEntityManager.
 */
public interface ScopedEntityManager extends EntityManager, AutoCloseable {
	
	/**
	 * The Interface Transaction.
	 */
	@FunctionalInterface
	public interface Transaction {
		
		/**
		 * Execute.
		 */
		public void execute();
	}

	/**
	 * The Interface TransactionFunction.
	 *
	 * @param <T> the generic type
	 */
	@FunctionalInterface
	public interface TransactionFunction<T> {
		
		/**
		 * Execute.
		 *
		 * @return the t
		 */
		public T execute();
	}

	/**
	 * Execute transaction.
	 *
	 * @param t the t
	 */
	public void executeTransaction(Transaction t);

	/**
	 * Execute transaction.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 */
	public <T> T executeTransaction(TransactionFunction<T> t);

}