package com.partieljee.chupin.orm;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;


/**
 * The Class EntityManagerFactoryHandler.
 */
public class EntityManagerFactoryHandler implements InvocationHandler{
	
	/** The Constant createEntityManager. */
	private static final Method createEntityManager;
	
	/** The Constant createScopedEntityManager. */
	private static final Method createScopedEntityManager;
	
	/** The Constant logger. */
	private static final Logger logger;
	
	static {
		try {
			createEntityManager = EntityManagerFactory.class.getMethod("createEntityManager");
			createScopedEntityManager = ScopedEntityManagerFactory.class.getMethod("createScopedEntityManager");
			logger = 	Logger.getLogger(ScopedEntityManagerFactory.class.getCanonicalName());
		}catch(NoSuchMethodException e) {
			throw new ExceptionInInitializerError();
		}
	}
	
	/** The emf. */
	private final EntityManagerFactory emf;
	
	/**
	 * Instantiates a new entity manager factory handler.
	 *
	 * @param emf the emf
	 */
	public  EntityManagerFactoryHandler(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object, java.lang.reflect.Method, java.lang.Object[])
	 */
	@Override
	public Object invoke(Object o, Method method, Object[] os) throws Throwable {
		logger.entering(ScopedEntityManagerFactory.class.getCanonicalName(), method.getName(),os);
		Object result;
		
		try {
			if(method.equals(createScopedEntityManager)) {
				assert(os.length == 0);
				EntityManager target = (EntityManager) createEntityManager.invoke(emf, os);
				result = Proxy.newProxyInstance(
						ScopedEntityManagerFactory.class.getClassLoader(),
						new Class[] {ScopedEntityManager.class},
						new EntityManagerHandler(target)
						);
			}
			else {
				result = method.invoke(emf, os);
			}
		}catch(Throwable e) {
			logger.throwing(ScopedEntityManagerFactory.class.getCanonicalName(),method.getName(),e);
			throw e;
		}
		if(method.getReturnType().getClass().equals(Void.class.getClass())) {
			logger.exiting(ScopedEntityManagerFactory.class.getCanonicalName(), method.getName());
		}
		else {
			logger.exiting(ScopedEntityManagerFactory.class.getCanonicalName(), method.getName(),result);
		}
		return result;
	}
	
}
