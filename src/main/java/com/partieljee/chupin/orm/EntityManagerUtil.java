package com.partieljee.chupin.orm;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/**
 * The Class EntityManagerUtil.
 */
public class EntityManagerUtil {
	
	/** The Constant entityManagerFactory. */
	private static final EntityManagerFactory entityManagerFactory;

	static {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("myPU");
		}
		catch(Throwable ex) {
			System.err.println("Initial Sessionfactory creation failed" + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	/**
	 * Gets the entity manager.
	 *
	 * @return the entity manager
	 */
	public static EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

}
