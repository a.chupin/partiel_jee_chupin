package com.partieljee.chupin.orm;


import javax.persistence.EntityManagerFactory;


/**
 * A factory for creating ScopedEntityManager objects.
 */
public interface ScopedEntityManagerFactory extends EntityManagerFactory {
	
	/**
	 * Creates a new ScopedEntityManager object.
	 *
	 * @return the scoped entity manager
	 */
	public ScopedEntityManager createScopedEntityManager();
}