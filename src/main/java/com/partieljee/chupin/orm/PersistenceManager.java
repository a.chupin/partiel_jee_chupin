package com.partieljee.chupin.orm;


import java.lang.reflect.Proxy;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/**
 * The Class PersistenceManager.
 */
public class PersistenceManager {
	
	/** The Constant singleton. */
	private static final PersistenceManager singleton = new PersistenceManager();

	/** The semf. */
	protected ScopedEntityManagerFactory semf;

	/**
	 * Gets the single instance of PersistenceManager.
	 *
	 * @return single instance of PersistenceManager
	 */
	public static PersistenceManager getInstance() {
		return singleton;
	}

	/**
	 * Instantiates a new persistence manager.
	 */
	private PersistenceManager() {
	}

	/**
	 * Gets the scoped entity manager factory.
	 *
	 * @return the scoped entity manager factory
	 */
	public ScopedEntityManagerFactory getScopedEntityManagerFactory() {
		
		if (semf == null) {
			createScopedEntityManagerFactory();
			System.out.println(semf.getClass().getName());
		}
		return semf;
	}

	/**
	 * Close entity managerfactory.
	 */
	public void closeEntityManagerfactory() {
		if (semf != null) {
			semf.close();
			semf = null;
		}
	}

	/**
	 * Creates the scoped entity manager factory.
	 */
	private void createScopedEntityManagerFactory() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myPU");
		semf = (ScopedEntityManagerFactory) Proxy.newProxyInstance(ScopedEntityManagerFactory.class.getClassLoader(),
				new Class[] { ScopedEntityManagerFactory.class }, new EntityManagerFactoryHandler(emf));
	}
}
