package com.partieljee.chupin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The Class User.
 */
@Table
@Entity
public class Seller {

	/** The id seller. */
	@Id
	@Column(nullable = false)
	private Long id_user;
	
	/**
	 * Gets the id seller.
	 *
	 * @return the id seller
	 */
	public Long getId_seller() {
		return id_user;
	}
	
	/**
	 * Sets the id seller.
	 *
	 * @param id_seller the new id seller
	 */
	public void setId_seller(Long id_user) {
		this.id_user = id_user;
	}



	/**
	 * Instantiates a new seller.
	 */
	public Seller () {}
	
	/**
	 * Instantiates a new seller.
	 *
	 * @param id_seller the id seller
	 * @param name the name
	 * @param status the status
	 */
	public Seller (Long id_user) {
		this.id_user = id_user;
	}
	
}
