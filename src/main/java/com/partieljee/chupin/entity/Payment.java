package com.partieljee.chupin.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Payment.
 */
@Table
@Entity
public class Payment {

	
	/** The id payment. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long id_payment;
	
	/** The id seller. */
	private Long id_seller;
	
	/** The id buyer. */
	private Long id_buyer;
	
	/** The id currency. */
	private Long id_currency;
	
	/** The id bill. */
	private Long id_bill;
	
	/** The date payment. */
	private Date date_payment;
	

	/**
	 * Gets the id payment.
	 *
	 * @return the id payment
	 */
	public Long getId_payment() {
		return id_payment;
	}

	/**
	 * Sets the id payment.
	 *
	 * @param id_payment the new id payment
	 */
	public void setId_payment(Long id_payment) {
		this.id_payment = id_payment;
	}

	/**
	 * Gets the id seller.
	 *
	 * @return the id seller
	 */
	public Long getId_seller() {
		return id_seller;
	}

	/**
	 * Sets the id seller.
	 *
	 * @param id_seller the new id seller
	 */
	public void setId_seller(Long id_seller) {
		this.id_seller = id_seller;
	}

	/**
	 * Gets the id buyer.
	 *
	 * @return the id buyer
	 */
	public Long getId_buyer() {
		return id_buyer;
	}

	/**
	 * Sets the id buyer.
	 *
	 * @param id_buyer the new id buyer
	 */
	public void setId_buyer(Long id_buyer) {
		this.id_buyer = id_buyer;
	}

	/**
	 * Gets the id currency.
	 *
	 * @return the id currency
	 */
	public Long getId_currency() {
		return id_currency;
	}

	/**
	 * Sets the id currency.
	 *
	 * @param id_currency the new id currency
	 */
	public void setId_currency(Long id_currency) {
		this.id_currency = id_currency;
	}

	/**
	 * Gets the id bill.
	 *
	 * @return the id bill
	 */
	public Long getId_bill() {
		return id_bill;
	}

	/**
	 * Sets the id bill.
	 *
	 * @param id_bill the new id bill
	 */
	public void setId_bill(Long id_bill) {
		this.id_bill = id_bill;
	}

	/**
	 * Gets the date payment.
	 *
	 * @return the date payment
	 */
	public Date getDate_payment() {
		return date_payment;
	}

	/**
	 * Sets the date payment.
	 *
	 * @param date_payment the new date payment
	 */
	public void setDate_payment(Date date_payment) {
		this.date_payment = date_payment;
	}

	/**
	 * Instantiates a new payment.
	 */
	public Payment () {}
	
	/**
	 * Instantiates a new payment.
	 *
	 * @param id_payment the id payment
	 * @param id_seller the id seller
	 * @param id_buyer the id buyer
	 * @param id_currency the id currency
	 * @param id_bill the id bill
	 * @param date_payment the date payment
	 */
	public Payment (Long id_payment, Long id_seller, Long id_buyer, Long id_currency, Long id_bill, Date date_payment) {
		this.id_payment = id_payment;
		this.id_seller = id_seller;
		this.id_buyer = id_buyer;
		this.id_currency = id_currency;
		this.id_bill = id_bill;
	}
	
}
