package com.partieljee.chupin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Product.
 */
@Table
@Entity
public class Product {

	/** The id product. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long id_product;
	
	/** The id seller. */
	private Long id_seller;
	
	/** The name. */
	private String name;
	
	/** The stock. */
	private int stock;
	
	/** The price. */
	private double price;
	
	
	/**
	 * Gets the id product.
	 *
	 * @return the id product
	 */
	public Long getId_product() {
		return id_product;
	}
	
	/**
	 * Sets the id product.
	 *
	 * @param id_product the new id product
	 */
	public void setId_product(Long id_product) {
		this.id_product = id_product;
	}
	
	/**
	 * Gets the id seller.
	 *
	 * @return the id seller
	 */
	public Long getId_seller() {
		return id_seller;
	}
	
	/**
	 * Sets the id seller.
	 *
	 * @param id_seller the new id seller
	 */
	public void setId_seller(Long id_seller) {
		this.id_seller = id_seller;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the stock.
	 *
	 * @return the stock
	 */
	public int getStock() {
		return stock;
	}
	
	/**
	 * Sets the stock.
	 *
	 * @param stock the new stock
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	
	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Instantiates a new product.
	 */
	public Product () {}
	
	/**
	 * Instantiates a new product.
	 *
	 * @param id_product the id product
	 * @param id_seller the id seller
	 * @param name the name
	 * @param stock the stock
	 * @param price the price
	 */
	public Product (Long id_product, Long id_seller, String name, int stock, double price) {
		this.id_product = id_product;
		this.id_seller = id_seller;
		this.name = name;
		this.stock = stock;
		this.price = price;
	}
	
}
