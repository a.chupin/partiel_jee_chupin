package com.partieljee.chupin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The Class User.
 */
@Table(name="tuser")
@Entity
public class User {

	/** The id user. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long id_user;
	
	/** The name. */
	private String name;
	
	/** The status. */
	private String status;

	/**
	 * Gets the id user.
	 *
	 * @return the id user
	 */
	public Long getId_user() {
		return id_user;
	}
	
	/**
	 * Sets the id user.
	 *
	 * @param id_user the new id user
	 */
	public void setId_user(Long id_user) {
		this.id_user = id_user;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Instantiates a new user.
	 */
	public User () {}
	
	/**
	 * Instantiates a new user.
	 *
	 * @param id_user the id user
	 * @param name the name
	 * @param status the status
	 */
	public User (Long id_user, String name, String status) {
		this.id_user = id_user;
		this.name = name;
		this.status = status;
	}
	
}
