package com.partieljee.chupin.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.partieljee.chupin.orm.ScopedEntityManagerFactory;

/**
 * The Class Order.
 */
@Table(name="torder")
@Entity
public class Order {

	/** The id order. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long id_order;
	
	/** The id buyer. */
	private Long id_buyer;
	
	/** The date order. */
	private Date date_order;
	
	

	/**
	 * Gets the id order.
	 *
	 * @return the id order
	 */
	public Long getId_order() {
		return id_order;
	}

	/**
	 * Sets the id order.
	 *
	 * @param id_order the new id order
	 */
	public void setId_order(Long id_order) {
		this.id_order = id_order;
	}

	/**
	 * Gets the id buyer.
	 *
	 * @return the id buyer
	 */
	public Long getId_buyer() {
		return id_buyer;
	}

	/**
	 * Sets the id buyer.
	 *
	 * @param id_buyer the new id buyer
	 */
	public void setId_buyer(Long id_buyer) {
		this.id_buyer = id_buyer;
	}

	/**
	 * Gets the date order.
	 *
	 * @return the date order
	 */
	public String getDate_order() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		return dateFormat.format(date_order);
	}

	/**
	 * Sets the date order.
	 *
	 * @param date_order the new date order
	 */
	public void setDate_order(String date_order) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		try {
			this.date_order = dateFormat.parse(date_order);
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ScopedEntityManagerFactory.class.getCanonicalName());
			logger.warning(e.toString());
		}
		
	}

	/**
	 * Instantiates a new order.
	 */
	public Order () {}
	
	/**
	 * Instantiates a new order.
	 *
	 * @param id_order the id order
	 * @param id_buyer the id buyer
	 * @param date_order the date order
	 */
	public Order (Long id_order, Long id_buyer, String date_order) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		
		this.id_order = id_order;
		this.id_buyer = id_buyer;
		try {
			this.date_order = dateFormat.parse(date_order);
		} catch (Exception e) {
			Logger logger = Logger.getLogger(ScopedEntityManagerFactory.class.getCanonicalName());
			logger.warning(e.toString());
		}
	}
	
}
