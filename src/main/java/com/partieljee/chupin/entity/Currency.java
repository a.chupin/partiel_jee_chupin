package com.partieljee.chupin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The Class Currency.
 */
@Table
@Entity
public class Currency {

	/** The id currency. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long id_currency;
	
	/** The currency. */
	private double currency;
	
	

	/**
	 * Gets the id currency.
	 *
	 * @return the id currency
	 */
	public Long getId_currency() {
		return id_currency;
	}

	/**
	 * Sets the id currency.
	 *
	 * @param id_currency the new id currency
	 */
	public void setId_currency(Long id_currency) {
		this.id_currency = id_currency;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public double getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(double currency) {
		this.currency = currency;
	}

	/**
	 * Instantiates a new currency.
	 */
	public Currency () {}
	
	/**
	 * Instantiates a new currency.
	 *
	 * @param id_currency the id currency
	 * @param currency the currency
	 */
	public Currency (Long id_currency, double currency) {
		this.id_currency = id_currency;
		this.currency = currency;
	}
	
}
