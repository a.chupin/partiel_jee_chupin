package com.partieljee.chupin.test;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import com.partieljee.chupin.entity.Product;
import com.partieljee.chupin.service.ProductService;


public class testService {

	public static interface SomeService {
		String getSomethingById(int id);
	}

	@Path("something")
	public static class SomethingResource {

		private final SomeService service;

		@Inject
		public SomethingResource(SomeService service) {
			this.service = service;
		}
		@GET
		@Path("{id}")
		public Response getSomethingById(@PathParam("id") int id) {
			String result = service.getSomethingById(id);
			return Response.ok(result).build();
		}
	}

	private SomethingResource resource;
	private SomeService service;

	@BeforeAll
	public void setUp() {
		service = Mockito.mock(SomeService.class);
	
		resource = new SomethingResource(service);
	}
	
	@Test
	public void testProductService() {
		//GIVEN
		Product p = Mockito.mock(Product.class);
		Mockito.when(p == null).then(invocation -> {
			invocation.callRealMethod();
			Object[] objects = invocation.getArguments();
			Object mock = invocation.getMock();
			return true;
		});
		//WHEN
		ProductService ps = new ProductService();
		ps.createProduct(p);
		boolean result = true;
		//THEN
		if(p == null)
			System.out.println("test ok");
		assertFalse(result);
	}
}
