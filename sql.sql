CREATE TABLE tuser (
	id_user			SERIAL PRIMARY KEY,
    name			VARCHAR(50),
    status			VARCHAR(30)
);

CREATE TABLE buyer (
	id_user			INTEGER PRIMARY KEY,
  	CONSTRAINT fk_buyer_user FOREIGN KEY (id_user) REFERENCES tuser(id_user)
);

CREATE TABLE seller (
	id_user			INTEGER PRIMARY KEY,
  	CONSTRAINT fk_seller_user FOREIGN KEY (id_user) REFERENCES tuser(id_user)
);

CREATE TABLE product (
	id_product		SERIAL PRIMARY KEY,
	id_seller		INTEGER NOT NULL,
	name			VARCHAR(100),
    stock			INTEGER,
    price			DOUBLE PRECISION,
	CONSTRAINT fk_product_seller FOREIGN KEY (id_seller) REFERENCES seller(id_user) 
);

CREATE TABLE torder (
	id_order		SERIAL PRIMARY KEY,
	id_buyer			INTEGER NOT NULL,
    date_order		DATE,
	CONSTRAINT fk_order_buyer FOREIGN KEY (id_buyer) REFERENCES buyer(id_user) 
);

CREATE TABLE productorder (
    id_productorder SERIAL PRIMARY KEY,
	id_product		INTEGER NOT NULL,
    id_order		INTEGER NOT NULL,
    quantity		INTEGER NOT NULL,
    CONSTRAINT fk_productorder_product FOREIGN KEY (id_product) REFERENCES product(id_product),
	CONSTRAINT fk_productorder_order FOREIGN KEY (id_order) REFERENCES torder(id_order)
);

CREATE TABLE currency (
	id_currency		SERIAL PRIMARY KEY,
    currency		MONEY
);

CREATE TABLE payment (
	id_payment		SERIAL PRIMARY KEY,
    id_seller		INTEGER NOT NULL,
    id_buyer		INTEGER NOT NULL,
    id_currency		INTEGER NOT NULL,
    id_bill			INTEGER NOT NULL,
    date_payment	DATE NOT NULL,
  	CONSTRAINT fk_payment_seller FOREIGN KEY (id_seller) REFERENCES seller(id_user),
    CONSTRAINT fk_payment_buyer FOREIGN KEY (id_buyer) REFERENCES buyer(id_user),
    CONSTRAINT fk_payment_currency FOREIGN KEY (id_currency) REFERENCES currency(id_currency)
);

CREATE TABLE bill (
	id_bill			SERIAL PRIMARY KEY,
    id_order INTEGER NOT NULL,
    CONSTRAINT fk_bill_order FOREIGN KEY (id_order) REFERENCES torder(id_order)
);



